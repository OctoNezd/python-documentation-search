#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from uuid import uuid4

from telegram import InlineQueryResultArticle, InputTextMessageContent, ParseMode, InlineKeyboardButton, \
    InlineKeyboardMarkup
from telegram.ext import Updater, InlineQueryHandler, CommandHandler

import indexer
import settings

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=settings.LOG_LEVEL)
# Silence the loud loggers
for item in ["telegram.ext.dispatcher", "telegram.bot", "telegram.vendor.ptb_urllib3.urllib3.connectionpool",
             "telegram.ext.updater"]:
    logging.getLogger(item).setLevel(logging.INFO)

logger = logging.getLogger(__name__)

EXAMPLES = {"executing code": "print('Hello world')",
            "checking PyPI": "requests",
            "checking official python documentation": "dict"}

EXAMPLES_KEYBOARD = []
for text, switch in EXAMPLES.items():
    EXAMPLES_KEYBOARD.append([InlineKeyboardButton(text="Try " + text, switch_inline_query_current_chat=switch)])
EXAMPLES_KEYBOARD = InlineKeyboardMarkup(EXAMPLES_KEYBOARD)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi! I am bot designed to be used in assisting someone with python by pointing them to '
                              'documentation or giving them code example.\nYou can try me out using buttons below!',
                              reply_markup=EXAMPLES_KEYBOARD)


def inlinequery(update, context):
    """Handle the inline query."""
    query = update.inline_query.query
    results = []
    logger.debug("offset: %s", repr(update.inline_query.offset))
    if update.inline_query.offset is '':
        offset = 0
        count = 0
    else:
        offset = int(update.inline_query.offset.split("|")[0])
        count = int(update.inline_query.offset.split("|")[1])
    if offset == -1:
        return
    if not query == "":
        section, section_name, next_section_name, thumb_url, next_index, last_index = indexer.inline_search(query, offset)
        results = []
        logger.debug(section)
        for item, item_title, item_description, item_url in section:
            results.append(InlineQueryResultArticle(
                id=uuid4(),
                title=item_title,
                description=item_description,
                input_message_content=InputTextMessageContent(
                    item, parse_mode=ParseMode.HTML),
                url=item_url,
                thumb_url=thumb_url
            ))
        if len(results) == 0:
            results.append(InlineQueryResultArticle(id=uuid4(), title=section_name,
                                                    input_message_content=InputTextMessageContent(
                                                        "Nothing found in {}".format(section_name)),
                                                    description="Not found",
                                                    thumb_url=thumb_url))
        if next_index == -1:
            switch_pm_text = "How to use bot"
        else:
            switch_pm_text = "Loading {}, {}/{}".format(next_section_name, next_index + 1, last_index)
        logger.debug(switch_pm_text)
        update.inline_query.answer(results, cache_time=settings.CACHE_TIME,
                                   switch_pm_parameter="start",
                                   switch_pm_text=switch_pm_text,
                                   next_offset="{}|{}".format(next_index, len(results) + count))
        if count == 0 and next_index == -1:
            update.inline_query.answer([InlineQueryResultArticle(
                id=uuid4(),
                title="Nothing found",
                input_message_content=InputTextMessageContent(
                    "Nothing found"))], cache_time=settings.CACHE_TIME,
                next_offset="{-1-0}")
    else:
        update.inline_query.answer([], switch_pm_text="No query specified...", switch_pm_parameter="start")


def error_callback(update, context):
    raise context.error
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(settings.TOKEN, use_context=True, **settings.UPDATER_KWARGS)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler(["start", "help"], start))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(InlineQueryHandler(inlinequery))

    # log all errors
    dp.add_error_handler(error_callback)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    logger.info("Start idling")
    updater.idle()


if __name__ == '__main__':
    main()
