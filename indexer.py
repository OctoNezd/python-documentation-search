import html
import logging
import os

import requests
from bs4 import BeautifulSoup
from sphobjinv import Inventory
from whoosh.fields import *
from whoosh.index import create_in, open_dir
from whoosh.qparser import QueryParser
from whoosh.query import Term

from utils import MWT

schema = Schema(title=TEXT(stored=True), path=ID(stored=True), source=TEXT(stored=True), domain=TEXT(stored=True),
                priority=NUMERIC(sortable=True), parent=TEXT(stored=True), is_parent=BOOLEAN(stored=True),
                inner_name=TEXT(stored=True))
s = requests.Session()
s.headers['User-Agent'] = 'PyDoc/1.0'
LOGGER = logging.getLogger("Indexer")


def search_engine(func):
    def func_wrapper(*args, **kwargs):
        func_res, searcher, index = func(*args, **kwargs)
        results = []
        for search_hit in func_res:
            item = {"title": search_hit["title"], "uri": search_hit["path"], "short_name": search_hit["title"]}
            if not search_hit["is_parent"]:
                parent_query = QueryParser("inner_name", index.schema).parse(search_hit["parent"])
                parent_results = searcher.search(parent_query,
                                                 filter=Term("is_parent", True))
                if len(parent_results) > 0:
                    name = parent_results[0]["title"] + " - " + search_hit["title"]
                    item["title"] = name
            r = requests.get(item["uri"])
            soup = BeautifulSoup(r.content, features='html.parser')
            item["description"] = ''
            if search_hit["is_parent"]:
                sf = soup.find("p")
                if sf:
                    item["description"] = html.escape(sf.text).replace("¶", "").strip("\n")
            else:
                sf = soup.find(id=search_hit["inner_name"])
                if sf:
                    item["description"] = sf.text.replace("¶", "").strip("\n")
            if item["description"] and len(item["description"]) > 410:
                item["description"] = item["description"][:410] + "..."
            item["extra"] = ""
            if sf:
                sibling = sf.find_next("dd")
                if sibling:
                    item["extra"] = html.escape(sibling.text)
            if len(item["extra"]) > 410:
                item["extra"] = item["extra"].replace("\n", "")[:410] + "..."
            results.append((
                f'<b>{html.escape(item["short_name"])}</b> (<a href="{search_hit["path"]}">{html.escape(item["title"])}</a>)\n'
                f'<code>{html.escape(item["description"])}</code>\n\n' + item["extra"]
                ,
                item["title"],
                item['description'], item["uri"]))
        return results

    return func_wrapper


def parse_obj_inv(source, data, writer):
    inv = Inventory(source=data)
    for obj in inv.objects:
        if not obj.dispname == "-":
            name = "%s (%s)" % (obj.dispname, obj.name)
        else:
            name = obj.name
        uri = source + obj.uri.replace("$", obj.name)
        is_parent = False
        if obj.uri.endswith("#$"):
            parent = obj.uri.split(".")[0]
        else:
            if not obj.dispname == "-":
                is_parent = True
            parent = "-"
        LOGGER.debug("Adding %s object to index for %s", name, source)
        writer.add_document(title=name, path=uri, source=source, domain=obj.domain, priority=obj.priority,
                            is_parent=is_parent, parent=parent.split("/")[-1], inner_name=obj.name.split("/")[-1])


@MWT(60 * 60 * 3)
def init_library_index(name, source):
    if os.path.exists("lib_indexes/" + name + ".inv") and os.path.exists("lib_indexes/" + name):
        with open("lib_indexes/" + name + ".inv", 'rb') as f:
            local = f.read()
        remote = s.get(source + "objects.inv").content
        if local == remote:
            LOGGER.debug("We have latest version of objects.inv of %s", name)
            update_required = False
        else:
            LOGGER.debug("Server-side has newer version of objects.inv of %s", name)
            update_required = True
    else:
        update_required = True
        remote = s.get(source).content
    if update_required:
        LOGGER.info("Creating index for %s from %s", name, source)
        os.makedirs("lib_indexes/" + name, exist_ok=True)
        with open("lib_indexes/" + name + ".inv", 'wb') as f:
            f.write(remote)
        ix = create_in("lib_indexes/" + name, schema)
        writer = ix.writer()
        parse_obj_inv(source, remote, writer)
        writer.commit()
    else:
        LOGGER.info("Found index for %s from %s", name, source)
        ix = open_dir("lib_indexes/" + name)
    return ix, ix.searcher()


@MWT(60 * 60 * 3)
@search_engine
def python_3_stdlib(query):
    ix, searcher = init_library_index("stdlib", "https://docs.python.org/3/")
    query = QueryParser("title", ix.schema).parse(query)
    results = searcher.search(query)
    return results, searcher, ix


@MWT(60 * 60 * 3)
def good_rtd(name):
    r = s.get("https://readthedocs.org/api/v2/project", params={"slug": name}).json()
    if r["count"] > 0:
        if r["results"][0]["programming_language"] == "py":
            return r["results"][0]["canonical_url"]
    return False


@MWT(60 * 60 * 3)
@search_engine
def python_3_read_the_docs(library, query):
    if library not in ["python", "stdlib"]:
        library_url = good_rtd(library)
        if library_url:
            index, searcher = init_library_index(library, library_url)
            if query == "":
                query = "*"
            query = QueryParser("title", index.schema).parse(query)
            results = searcher.search(query)
            return results, searcher, index
    return [], None, None


def good_package(strg, regex_search=re.compile(r'[^a-zA-Z\-_0-9]').search):
    return not bool(regex_search(strg))


@MWT(60 * 60 * 3)
def get_pypi(query):
    if not good_package(query):
        return []
    else:
        r = s.get("https://pypi.org/pypi/%s/json" % query)
        if r.ok:
            package = r.json()['info']
            message = []
            message.append(f"{package['name']} by {html.escape(package['author'])}")
            message.append(f"<i>{html.escape(package['summary'])}</i>")
            message.append('Classifiers:' + html.escape("\n- ".join(package["classifiers"])))
            message.append('Installation:\n\n'
                           '- Python 2:\n'
                           f' - <code>python2 -m pip install {html.escape(package["name"])}</code>\n\n'
                           '- Python 3:\n'
                           f' - <code>python3 -m pip install {html.escape(package["name"])}</code>\n\n')
            return [("\n".join(message),
                     f"{package['name']} by {html.escape(package['author'])}",
                     html.escape(package["summary"]),
                     "https://pypi.org/pypi/%s" % query)]
        else:
            return []


@MWT(60 * 60 * 3)
def rextester(code):
    r = s.post("https://rextester.com/rundotnet/api", data={"LanguageChoice": "24", "Program": code}).json()
    r["Source code"] = code
    msg = []
    for k, v in r.items():
        if v:
            msg.append(f"<b>{k}:</b><pre>{html.escape(v)}</pre>")
    if "Errors" in r and r["Errors"]:
        desc = "Errors were raised"
    elif "Result" in r and r["Result"]:
        desc = "Output:" + r["Result"].split("\n")[0]
    else:
        desc = "Execution successful"
    return [("\n".join(msg), "Executed code", desc, None)]


def rtfd_search(query):
    if len(query.split()) == 1 and len(query.split(".")):
        return python_3_read_the_docs(query.split(".")[0], query)
    else:
        return python_3_read_the_docs(query.split()[0], " ".join(query.split()[1:]))


def pypi_search(query):
    if len(query.split()) == 1 and len(query.split(".")):
        return get_pypi(query.split(".")[0])
    else:
        return get_pypi(query.split()[0])


FUNCS = [[rextester, "Rextester", "https://cdn2.iconfinder.com/data/icons/font-awesome/1792/code-512.png"],
         [pypi_search, "PyPI", "https://pypi.org/static/images/twitter.c0030826.jpg"],
         [rtfd_search, "RTFD", "https://read-the-docs-guidelines.readthedocs-hosted.com/_images/logo-light.png"],
         [python_3_stdlib, "stdlib",
          "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1024px-Python-logo-notext.svg.png"]
        ]


def inline_search(query, index=0):
    if index > len(FUNCS) - 1:
        return None, None, None, -1, len(FUNCS)
    else:
        LOGGER.debug("Running %s", FUNCS[index][0])
        if index + 1 > len(FUNCS) - 1:
            next_index = -1
            next_func = None
        else:
            next_index = index + 1
            next_func = FUNCS[next_index][1]
        return FUNCS[index][0](query), FUNCS[index][1], next_func, FUNCS[index][2], next_index, len(FUNCS)


# Just so stdlib is cached as soon as bot starts up
init_library_index("stdlib", "https://docs.python.org/3/")
