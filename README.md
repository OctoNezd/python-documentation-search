# Python documentation search bot for Telegram
1. Create venv cause we are cool
```
python3 -m virtualenv venv
```
2. Switch into it
```
venv/Scripts/activate
```
or
```
venv/bin/activate
```
3. Install requirements
```
python3 -m pip install requirements.txt
```
4. Copy settings file
```
cp settings_example.py settings.py
```
5. Run bot!
```
python3 bot.py
```